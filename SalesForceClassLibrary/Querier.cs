﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesForceClassLibrary
{
    public class Querier <T> where T : SalesforceEnterprise.sObject
    {
        private EnterpriseService _enterpriseService = null;


        public Querier()
        {
            _enterpriseService = new EnterpriseService();
        }

        public Querier(EnterpriseService eService)
        {
            _enterpriseService = eService;
        }

        public List<T> Query(string query)
        {
            List<T> results = new List<T>();
            SalesforceEnterprise.QueryResult sfQueryResult = this._enterpriseService.query(query);

            if (sfQueryResult == null || sfQueryResult.size == 0)
            {
                return results;
            }

            results.AddRange(sfQueryResult.records.OfType<T>().ToList());

            //addQueryResults(results, sfQueryResult);
            while (sfQueryResult.done == false)
            {
                sfQueryResult = this._enterpriseService.queryMore(sfQueryResult);
                //addQueryResults(results, sfQueryResult);
                results.AddRange(sfQueryResult.records.OfType<T>().ToList());
                
                // Switched order by SK & GC. Original code was appending results before running the new query
                //addQueryResults(results, sfQueryResult);
                //sfQueryResult = this._enterpriseService.queryMore(sfQueryResult);
            }

            return results;           
        }
        

        private void _dep_addQueryResults(List<T> results, SalesforceEnterprise.QueryResult queryResult)
        {
            for (int i=0; i<queryResult.records.Length; i++)
            {
                T SfResult = (T) queryResult.records[i];
                results.Add(SfResult);
            }
        }

    }
}
