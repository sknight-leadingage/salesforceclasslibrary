﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;

namespace SalesForceClassLibrary
{
    public static class BulkData
    {
        public const string BulkCSVFieldChar = "¸";

        public enum BulkTableAction
        {
            DROP,
            TRUNCATE,
            APPEND
        }

        public enum BulkSubqueryOutput
        {
            SingleColumn,
            MultipleColumns
        }

        public class BulkServer
        {
            private string _sqlconn;
            private string _path;
            private string _soql;
            private string _table;
            private BulkTableAction _drop;
            private EnterpriseService _sf;
            public BulkQueryData QueryHelper;
            private BulkSubqueryOutput _subquery;

            public BulkServer(string SQLConnection, string BulkDataPath, string BulkDataTableName, BulkTableAction TableAction, string SOQLQuery, EnterpriseService SalesforceOrg)
            {
                this._sqlconn = SQLConnection;
                this._path = BulkDataPath;
                this._soql = SOQLQuery;
                this._sf = SalesforceOrg;
                this._table = BulkDataTableName;
                this._drop = TableAction;
                this.QueryHelper = new BulkQueryData(SOQLQuery);
                this._subquery = BulkSubqueryOutput.SingleColumn;
            }

            public BulkServer(string SQLConnection, string BulkDataPath, string BulkDataTableName, BulkTableAction TableAction, string SOQLQuery, EnterpriseService SalesforceOrg, BulkSubqueryOutput SubqueryOutput)
            {
                this._sqlconn = SQLConnection;
                this._path = BulkDataPath;
                this._soql = SOQLQuery;
                this._sf = SalesforceOrg;
                this._table = BulkDataTableName;
                this._drop = TableAction;
                this.QueryHelper = new BulkQueryData(SOQLQuery);
                this._subquery = SubqueryOutput;
            }

            public string SQLConnection
            {
                get { return this._sqlconn; }
                set { this._sqlconn = value; }
            }

            public string BulkDataPath
            {
                get { return this._path; }
                set { this._path = value; }
            }

            public string SOQLQuery
            {
                get { return this._soql; }
                set { this._soql = value; }
            }

            public EnterpriseService SalesforceOrg
            {
                get { return this._sf; }
                set { this._sf = value; }
            }

            public string BulkDataTableName
            {
                get { return this._table; }
                set { this._table = value; }
            }

            public BulkTableAction BulkDataTableAction
            {
                get { return this._drop; }
                set { this._drop = value; }
            }

            public BulkSubqueryOutput BulkSubqueryOutput
            {
                get { return this._subquery; }
                set { this._subquery = value; }
            }
        }

        public class BulkReturn
        {
            private string _m;
            private bool _b;
            
            public BulkReturn(bool Success, string Message)
            {
                this._b = Success;
                this._m = Message;
            }

            public bool Success
            {
                get { return this._b; }
                set { this._b = value; }
            }

            public string Message
            {
                get { return this._m; }
                set { this._m = value; }
            }
        }

        /*
         * Example of how to use BulkServer and the DownloadBulkDataSalesforceToSQL method with delegate:
         * Simon 20130521
         * 
            BulkData.BulkServer ep = new BulkData.BulkServer(
                ConfigurationManager.ConnectionStrings["LyrisDev"].ConnectionString,
                @"\\LeadingAge-db02\c$\temp\LyrisNimbleBaseImport.csv",
                "LyrisNimbleBaseImport",
                false, //truncate table
                String.Format("SELECT Account__r.PersonEmail,Product__r.Online_Subscription_Type__c,Product__r.Lyris_List_Name__c,Product__r.Name,Listserv_Delivery_Option__c,Status__c FROM Online_Subscription__c WHERE Id IN (SELECT ParentId FROM Online_Subscription__History WHERE CreatedDate = LAST_N_DAYS:{0}) AND Status__c != 'Held'", 31),
                new EnterpriseService()
            );
            BulkData.BulkReturn br = BulkData.DownloadBulkDataSalesforceToSQL(
                ep,
                new SalesForceClassLibrary.BulkData.BulkSaveAction(
                    delegate (SalesForceClassLibrary.SalesforceEnterprise.sObject o)
                    {
                        string strRetVal = "";
                        SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c sfSub = (SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c)o;
                        if (sfSub != null && sfSub.Product__r != null && sfSub.Account__r != null && SalesforceValidation.Account.IsLikeAnEmail(sfSub.Account__r.PersonEmail))
                        {
                            strRetVal = String.Format("{0},{1},{2},{3}", SalesforceValidation.SafeGetObjectField(sfSub.Account__r.PersonEmail), SalesforceValidation.SafeGetObjectField(sfSub.Product__r.Lyris_List_Name__c), SalesforceValidation.SafeGetObjectField(sfSub.Status__c), SalesforceValidation.Listserv.GetDeliveryOptionWithDefault(sfSub, SalesforceValidation.Listserv.DeliveryOptions.Immediate_Delivery));
                        }
                        return strRetVal;
                    }
                )
            );
         */

        public delegate string BulkSaveAction(SalesForceClassLibrary.SalesforceEnterprise.sObject DataRow);

        public static BulkReturn DownloadBulkDataSalesforceToSQL(BulkServer Endpoints, BulkSaveAction e)
        {
            BulkReturn b = new BulkReturn(false, "Operation Ended Without Executing");

            try
            {
                Querier<SalesForceClassLibrary.SalesforceEnterprise.sObject> q = new Querier<SalesForceClassLibrary.SalesforceEnterprise.sObject>(Endpoints.SalesforceOrg);
                List<SalesForceClassLibrary.SalesforceEnterprise.sObject> lq = q.Query(Endpoints.SOQLQuery);
                if (lq.Count > 0)
                {
                    long iRecs = 0;
                    string strWrite = "";
                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(Endpoints.BulkDataPath, false, Encoding.Default))
                    {
                        foreach (SalesForceClassLibrary.SalesforceEnterprise.sObject soItem in lq)
                        {
                            if (!string.IsNullOrEmpty((strWrite = e(soItem))))
                            {
                                sw.WriteLine(strWrite);
                                iRecs++;
                            }
                        }
                    }
                    if (iRecs > 0)
                    {
                        using (SqlConnection sqlConn = new SqlConnection(Endpoints.SQLConnection))
                        {
                            SqlCommand sqlCmd = new SqlCommand("", sqlConn);
                            if (Endpoints.BulkDataTableAction == BulkTableAction.DROP)
                            {
                                sqlCmd.CommandText = String.Format("IF EXISTS(SELECT [name] FROM dbo.sysobjects WHERE [name] = '{0}') DROP TABLE {1}; ", Endpoints.BulkDataTableName, Endpoints.BulkDataTableName);
                            }
                            else if (Endpoints.BulkDataTableAction == BulkTableAction.TRUNCATE)
                            {
                                sqlCmd.CommandText = String.Format("IF EXISTS(SELECT [name] FROM dbo.sysobjects WHERE [name] = '{0}') TRUNCATE TABLE {1}; ", Endpoints.BulkDataTableName, Endpoints.BulkDataTableName);
                            }
                            string strCrTable = Endpoints.QueryHelper.GetSQLCreateTable(Endpoints.BulkDataTableName);
                            if (!String.IsNullOrEmpty(strCrTable))
                            {
                                sqlCmd.CommandText += String.Format("IF NOT EXISTS(SELECT [name] FROM dbo.sysobjects WHERE [name] = '{0}') BEGIN{2}{1}{2}END; ", Endpoints.BulkDataTableName, strCrTable, System.Environment.NewLine);
                            }
                            sqlCmd.CommandText += String.Format(@"BULK INSERT {0} FROM '{1}' WITH (FIELDTERMINATOR='{2}', ROWTERMINATOR='\n');", Endpoints.BulkDataTableName, Endpoints.BulkDataPath, BulkData.BulkCSVFieldChar);

                            sqlCmd.Connection.Open();
                            sqlCmd.ExecuteNonQuery();
                            sqlCmd.Connection.Close();

                            b.Success = true;
                            b.Message = String.Format("Data Processed Successfully With Record Count: {0}", iRecs);
                        }
                    }
                    else
                    {
                        b.Success = true;
                        b.Message = String.Format("Salesforce Connection Successful but no valid records returned by SOQL Query: {0}", Endpoints.SOQLQuery);
                    }
                }
                else
                {
                    b.Success = true;
                    b.Message = String.Format("Salesforce Connection Successful but no records returned by SOQL Query: {0}", Endpoints.SOQLQuery);
                }
            }
            catch (Exception ex)
            {
                b.Success = false;
                b.Message = ex.Message.ToString();
            }

            return b;
        }

        public class BulkQueryData
        {
            public Dictionary<string, List<string>> FieldMap = new Dictionary<string, List<string>>();
            public Dictionary<string, int> SQLColumns = new Dictionary<string, int>();

            private BulkData.BulkSubqueryOutput _subquery = BulkSubqueryOutput.SingleColumn;

            public BulkQueryData(BulkServer Endpoints)
            {
                _subquery = Endpoints.BulkSubqueryOutput;
                PerformBulkQuery(Endpoints.SOQLQuery);
            }

            public BulkQueryData(string SOQL)
            {
                PerformBulkQuery(SOQL);
            }

            internal void PerformBulkQuery(string SOQL)
            {
                string strTemp = SOQL;
                strTemp = Regex.Replace(strTemp, @"\(.+?\)", "", RegexOptions.IgnoreCase);

                Regex rx1 = new Regex(@"SELECT(.+?)FROM(.+?)(?:WHERE.+|\Z|$)", RegexOptions.IgnoreCase);
                MatchCollection mc = rx1.Matches(strTemp);
                if (mc.Count == 1)
                {
                    //Should only be one match (We eliminated subqueries at this point, so there should only be one "parent" / main query, e.g. THE QUERY
                    if (mc[0].Groups.Count == 3)
                    {
                        //Remember, 0 (first group match) is the whole string, then 1 is first captured group and 2 is 2nd
                        List<string> t = mc[0].Groups[1].Value.Trim().Split(",".ToCharArray()).ToList<string>();
                        //t.ForEach(x => x = x.Trim());
                        t = t.Select(x => x = x.Trim()).ToList<string>();
                        t.RemoveAll(x => string.IsNullOrEmpty(x));
                        this.FieldMap.Add(mc[0].Groups[2].Value.Trim(), t);
                    }

                    strTemp = SOQL;

                    int iIN = 0;
                    while ((iIN = strTemp.ToLower().LastIndexOf("in (")) > 0)
                    {
                        strTemp = strTemp.Substring(0, iIN - 1).Trim();
                    }

                    while ((iIN = strTemp.ToLower().LastIndexOf("in(")) > 0)
                    {
                        strTemp = strTemp.Substring(0, iIN - 1).Trim();
                    }

                    int i = strTemp.ToLower().LastIndexOf("where");
                    if (i > 1)
                    {
                        strTemp = strTemp.Substring(0, i - 1).Trim();
                        Regex rx2 = new Regex(@"\((.+?)\)", RegexOptions.IgnoreCase);
                        Regex rx3 = new Regex(@"SELECT\s+(.+?)\s+FROM\s+(.+?)(?:\s+(?:WHERE|ORDER\s*BY)\s+.+|\Z|$)", RegexOptions.IgnoreCase);
                        if (rx2.IsMatch(strTemp))
                        {
                            foreach (Match m in rx2.Matches(strTemp))
                            {
                                //Foreach possible subquery in the original query
                                if (m.Groups.Count > 1)
                                {
                                    MatchCollection mc1 = rx3.Matches(m.Groups[1].ToString());
                                    if (mc1.Count == 1)
                                    {
                                        //table and fieldlist for subqueries
                                        if (mc1[0].Groups.Count == 3)
                                        {
                                            //Remember, 0 (first group match) is the whole string, then 1 is first captured group and 2 is 2nd
                                            //.ForEach(x => x.Trim())
                                            List<string> t = mc1[0].Groups[1].Value.Trim().Split(",".ToCharArray()).ToList<string>();
                                            //t.ForEach(x => x = x.Trim());
                                            t = t.Select(x => x = x.Trim()).ToList<string>();
                                            t.RemoveAll(x => string.IsNullOrEmpty(x));
                                            this.FieldMap.Add(mc1[0].Groups[2].Value.Trim(), t);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            internal string TruncateColumnName(string Col)
            {
                if (Col.Length > 128)
                {
                    return Col.Substring(0, 128);
                }
                else
                {
                    return Col;
                }
            }

            internal string FixColumnValue(int Val)
            {
                if (Val <= 0)
                {
                    return "1";
                }
                else if (Val > 1000)
                {
                    return "MAX";
                }
                else
                {
                    return Val.ToString();
                }
            }

            public string GetSQLCreateTable(string TableName)
            {
                string strRetVal = "";
                if (this.SQLColumns.Count > 0)
                {
                    strRetVal = String.Format("CREATE TABLE {0}{1} ( ", TableName, Environment.NewLine);
                    foreach (KeyValuePair<string, int> kvpItem in this.SQLColumns)
                    {
                        strRetVal += String.Format("{0}{1}    NVARCHAR({2}) NULL,", Environment.NewLine, TruncateColumnName(kvpItem.Key), FixColumnValue(kvpItem.Value == 0 ? 1 : kvpItem.Value));
                    }
                    strRetVal = strRetVal.Substring(0, strRetVal.Length - 1);
                    strRetVal += String.Format("{0} );", Environment.NewLine);
                }
                return strRetVal;
            }

            public enum HandleSpecialChars
            {
                RemoveLinebreaks,
                ReplaceLinebreaksWithPlaintext
            }

            public string GetAllFieldsAsCSV(SalesForceClassLibrary.SalesforceEnterprise.sObject o)
            {
                return this.GetAllFieldsAsCSV(o, HandleSpecialChars.RemoveLinebreaks);
            }

            public string GetAllFieldsAsCSV(SalesForceClassLibrary.SalesforceEnterprise.sObject o, HandleSpecialChars SpCActions)
            {
                string strRetVal = "";
                string strItem = "";
                dynamic Item = null;
                int ki = 0;

                string strLbReplace = "";
                if (SpCActions == HandleSpecialChars.ReplaceLinebreaksWithPlaintext)
                {
                    strLbReplace = " ";
                }
                foreach (KeyValuePair<string, List<string>> kvpItem in this.FieldMap)
                {
                    if (ki == 0)
                    {
                        //First Query is "main" query
                        foreach (string strField in kvpItem.Value)
                        {
                            strItem = _GetAllFieldsAsCSV_GetItem(o, strField);
                            strItem = strItem.Replace("—", "-");
                            strItem = strItem.Replace("’", "'");
                            strRetVal += String.Format("{0}{1}", Regex.Replace(strItem, @"[\n\r]", strLbReplace, RegexOptions.IgnoreCase), BulkData.BulkCSVFieldChar);
                            this._GetAllFieldsAsCSV_AddSQLColumn(kvpItem.Key, strField, strItem.Length);
                        }
                    }
                    else
                    {
                        if (_subquery == BulkSubqueryOutput.SingleColumn)
                        {
                            //Taking all the fields/data for each "parent" row and compacting them into one column
                            string strSQFList = "";
                            //Everything else is subquery
                            if ((Item = GetPropValue(o, kvpItem.Key.Trim())) != null)
                            {
                                //Item should be a QueryResult with the subquery inside
                                SalesforceEnterprise.QueryResult t = new SalesforceEnterprise.QueryResult();
                                if (t.GetType().Equals(Item.GetType()))
                                {
                                    string strInnerItem = "";
                                    string strTempRet = "";
                                    string strTempRet1 = "";
                                    string strField = "";
                                    t = (SalesforceEnterprise.QueryResult)Item;
                                    foreach (SalesforceEnterprise.sObject s in t.records)
                                    {
                                        strTempRet = "";
                                        strSQFList = "";
                                        foreach (string strFieldIn in kvpItem.Value)
                                        {
                                            strField = strFieldIn.Trim();
                                            strInnerItem = _GetAllFieldsAsCSV_GetItem(s, strField);
                                            if (!string.IsNullOrEmpty(strInnerItem))
                                            {
                                                strTempRet += String.Format("{0}|", Regex.Replace(strInnerItem, @"[\n\r]", "", RegexOptions.IgnoreCase));
                                            }
                                            strSQFList = _GetAllFieldsAsCSV_AppendSubqueryName(strSQFList, strField);
                                        }
                                        if (strTempRet.Length - 1 > 0)
                                        {
                                            strTempRet = strTempRet.Substring(0, strTempRet.Length - 1);
                                        }
                                        else
                                        {
                                            strTempRet = "";
                                        }
                                        strTempRet1 += String.Format("{0};", strTempRet);
                                    }
                                    if (strTempRet1.Length - 1 > 0)
                                    {
                                        strTempRet1 = strTempRet1.Substring(0, strTempRet1.Length - 1);
                                        this._GetAllFieldsAsCSV_AddSQLColumn(kvpItem.Key, strSQFList, strTempRet1.Length);
                                    }
                                    else
                                    {
                                        strTempRet1 = "";
                                    }
                                    strRetVal += String.Format("{0}{1}", strTempRet1, BulkData.BulkCSVFieldChar);
                                }
                            }
                            else
                            {
                                //if the entire QueryResult for the subquery is missing then we *do* want to add a comma, to keep column counts the same
                                //we also want to include the subquery column in the SQL Create set
                                strRetVal += BulkData.BulkCSVFieldChar;
                                strSQFList = "";
                                foreach (string strField in kvpItem.Value)
                                {
                                    strSQFList = _GetAllFieldsAsCSV_AppendSubqueryName(strSQFList, strField.Trim());
                                }
                                this._GetAllFieldsAsCSV_AddSQLColumn(kvpItem.Key, strSQFList, 0);
                            }
                        }
                        else if (_subquery == BulkSubqueryOutput.MultipleColumns)
                        {
                            //Taking each row in the subquery and "pivoting" it so that each new row becomes an additional set of columns
                        }
                    }

                    ki++;
                }

                if (strRetVal.Length - 1 > 0)
                {
                    strRetVal = strRetVal.Substring(0, strRetVal.Length - 1);
                }
                else
                {
                    strRetVal = "";
                }

                return strRetVal;
            }

            internal string _GetAllFieldsAsCSV_AppendSubqueryName(string Parent, string Name)
            {
                if (String.IsNullOrEmpty(Parent))
                {
                    Parent = Name;
                }
                else
                {
                    Parent += "#" + Name;
                }
                return Parent;
            }

            internal string _GetAllFieldsAsCSV_GetItem(SalesforceEnterprise.sObject o, string FieldList)
            {
                string strRetVal = "";
                dynamic Item = null;
                dynamic InnerItem = null;
                dynamic InnerInnerItem = null;

                string[] xRs = FieldList.Trim().Split(".".ToCharArray());
                if (xRs.Length > 3 || xRs.Length <= 0)
                {
                    throw new Exception("Fatal Error: Too many relationship levels");
                }
                else
                {
                    if (xRs.Length == 1)
                    {
                        if ((Item = GetPropValue(o, xRs[0].Trim())) != null)
                        {
                            strRetVal = Item.ToString();
                        }
                    }
                    else if (xRs.Length == 2)
                    {
                        if ((Item = GetPropValue(o, xRs[0].Trim())) != null)
                        {
                            if ((InnerItem = GetPropValue(Item, xRs[1].Trim())) != null)
                            {
                                strRetVal = InnerItem.ToString();
                            }
                        }
                    }
                    else
                    {
                        if ((Item = GetPropValue(o, xRs[0].Trim())) != null)
                        {
                            if ((InnerItem = GetPropValue(Item, xRs[1].Trim())) != null)
                            {
                                //SalesforceEnterprise.QueryResult qri = new SalesforceEnterprise.QueryResult();
                                //qri = ((SalesforceEnterprise.QueryResult)InnerItem);

                                if ((InnerInnerItem = GetPropValue(InnerItem, xRs[2].Trim())) != null)
                                {
                                    strRetVal = InnerInnerItem.ToString();
                                }
                            }
                        }
                    }
                }
                return strRetVal;
            }

            internal void _GetAllFieldsAsCSV_AddSQLColumn(string BaseObject, string RelationshipAndOrFieldName, int DataLength)
            {
                string strSqlMapKey = "";
                strSqlMapKey = String.Format("{0}.{1}", BaseObject.Trim(), RelationshipAndOrFieldName.Trim()).Replace(".", "@");
                if (this.SQLColumns.ContainsKey(strSqlMapKey))
                {
                    if (DataLength > this.SQLColumns[strSqlMapKey])
                    {
                        this.SQLColumns[strSqlMapKey] = DataLength;
                    }
                }
                else
                {
                    this.SQLColumns.Add(strSqlMapKey, DataLength);
                }
            }

            internal static dynamic GetPropValue(SalesForceClassLibrary.SalesforceEnterprise.sObject Src, string PropName)
            {
                if (Src.GetType().GetProperty(PropName) != null)
                {
                    return (dynamic)Src.GetType().GetProperty(PropName).GetValue(Src, null);
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
