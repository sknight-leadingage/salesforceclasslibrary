﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesForceClassLibrary
{
    public static class QueryUtil
    {
        public static string Escape(string input)
        {
            return input.Replace("\\", "\\\\").Replace("'", "\\'");
        }
        
    }
}
