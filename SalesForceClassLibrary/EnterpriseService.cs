﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Runtime.Caching;


namespace SalesForceClassLibrary
{
    public class EnterpriseService
    {
        private string _password = String.Empty;
        private string _username = String.Empty;
        private string _serviceURL = String.Empty;
        private SalesforceEnterprise.SforceService _SFService = null;
        private SalesforceEnterprise.LoginResult _SFLoginResult = null;
        private Object _connectLock = new Object();

        public EnterpriseService()
        {
#if(DEBUG)
            this._password = ConfigurationManager.AppSettings["D_SfApiPassword"];
            this._username = ConfigurationManager.AppSettings["D_SfApiUserName"];
            this._serviceURL = ConfigurationManager.AppSettings["D_SFAPI_ServiceURL"];
#else
            this._password = ConfigurationManager.AppSettings["P_SfApiPassword"];
            this._username = ConfigurationManager.AppSettings["P_SfApiUserName"];
            this._serviceURL = ConfigurationManager.AppSettings["P_SFAPI_ServiceURL"];

            DateTime dtNow = DateTime.Now;
            if (dtNow.Minute >= 16 && dtNow.Minute <= 30)
            {
                if (ConfigurationManager.AppSettings.AllKeys.Contains("P_SfApiPassword2") && ConfigurationManager.AppSettings.AllKeys.Contains("P_SfApiUserName2"))
                {
                    this._password = ConfigurationManager.AppSettings["P_SfApiPassword2"];
                    this._username = ConfigurationManager.AppSettings["P_SfApiUserName2"];
                }
            }
            else if (dtNow.Minute >= 31 && dtNow.Minute <= 45)
            {
                if (ConfigurationManager.AppSettings.AllKeys.Contains("P_SfApiPassword3") && ConfigurationManager.AppSettings.AllKeys.Contains("P_SfApiUserName3"))
                {
                    this._password = ConfigurationManager.AppSettings["P_SfApiPassword3"];
                    this._username = ConfigurationManager.AppSettings["P_SfApiUserName3"];
                }
            }
            else if (dtNow.Minute >= 46 && dtNow.Minute <= 60)
            {
                if (ConfigurationManager.AppSettings.AllKeys.Contains("P_SfApiPassword4") && ConfigurationManager.AppSettings.AllKeys.Contains("P_SfApiUserName4"))
                {
                    this._password = ConfigurationManager.AppSettings["P_SfApiPassword4"];
                    this._username = ConfigurationManager.AppSettings["P_SfApiUserName4"];
                }
            }
#endif
            initSFService();
        }

        public string UsernameInUse
        {
            get
            {
                return this._username;
            }
        }


        private void initSFService()
        {
            this._SFService = new SalesforceEnterprise.SforceService();
            this._SFService.Timeout = 60000;
            this._SFService.QueryOptionsValue = new SalesforceEnterprise.QueryOptions();
            this._SFService.QueryOptionsValue.batchSize = 2000;
            this._SFService.QueryOptionsValue.batchSizeSpecified = true;

            string serverURL = MemoryCache.Default.Get("SFServerURL") as string;
            if (string.IsNullOrEmpty(serverURL) == false)
            {                
                this._SFService.Url = serverURL;
            }

            string loginSessionID = MemoryCache.Default.Get("SFSessionsID") as string;
            if (string.IsNullOrEmpty(serverURL) == false)
            {
                setSessionID(loginSessionID);
            }
        }

        private void setSessionID(string sessionID)
        {
            this._SFService.SessionHeaderValue = new SalesforceEnterprise.SessionHeader();
            this._SFService.SessionHeaderValue.sessionId = sessionID;
        }


        private void connect()
        {
            lock (this._connectLock) 
            {
                string sessionID = MemoryCache.Default.Get("SFSessionsID") as string;
                if (this._SFService != null && !string.IsNullOrEmpty(sessionID))
                {
                    return;
                }

                if (!string.IsNullOrEmpty(_serviceURL))
                {
                    this._SFService.Url = _serviceURL;
                }

                this._SFLoginResult = this._SFService.login(this._username, this._password);

                if (this._SFLoginResult.passwordExpired)
                {
                    throw new Exception("Your Password Has Expired!");
                }

                this._SFService.Url = this._SFLoginResult.serverUrl;
                setSessionID(this._SFLoginResult.sessionId);
                DateTimeOffset dtOffSet = DateTime.Now.AddMinutes(25);

                MemoryCache.Default.Add("SFSessionID", this._SFLoginResult.sessionId, dtOffSet);
                MemoryCache.Default.Add("SFServerURL", this._SFLoginResult.serverUrl, dtOffSet);
            }
        }

        public SalesforceEnterprise.QueryResult query (string query)
        {
            connect();
            return this._SFService.query(query);
        }


        public SalesforceEnterprise.QueryResult queryMore (SalesforceEnterprise.QueryResult result)
        {
            connect();
            return this._SFService.queryMore(result.queryLocator);
        }

        public SalesforceEnterprise.UpsertResult[] upsert(SalesforceEnterprise.sObject[] sObjectsToSave)
        {
            connect();

            SalesforceEnterprise.UpsertResult[] upsertResults = {};
            Dictionary<int, List<SalesforceEnterprise.sObject>> usChunks = new Dictionary<int, List<SalesforceEnterprise.sObject>>();
            double lChunks = (double)sObjectsToSave.LongLength / 200.0;
            if (lChunks > 1)
            {
                int iChunk = 0;
                long lChunk = 0;
                for (long i = 0; i < sObjectsToSave.LongLength; i++)
                {
                    if (usChunks.ContainsKey(iChunk))
                    {
                        usChunks[iChunk].Add(sObjectsToSave[i]);
                    }
                    else
                    {
                        usChunks.Add(iChunk, new List<SalesforceEnterprise.sObject>());
                        usChunks[iChunk].Add(sObjectsToSave[i]);
                    }

                    lChunk++;
                    if (lChunk == 200)
                    {
                        lChunk = 0;
                        iChunk++;
                    }
                }

                foreach (KeyValuePair<int, List<SalesforceEnterprise.sObject>> kvpItem in usChunks)
                {
                    upsertResults = _SFService.upsert("Id", kvpItem.Value.ToArray());
                }
            }
            else
            {
                upsertResults = _SFService.upsert("Id", sObjectsToSave);
            }

#if DEBUG
            string strErrors = "";
#endif
            for (int i = 0; i < upsertResults.Length; i++)
            {
                SalesforceEnterprise.UpsertResult result = upsertResults[i];
                if (result.success && result.created)
                {
                    SalesforceEnterprise.sObject insertedSObject = sObjectsToSave[i];
                    insertedSObject.Id = result.id;
                }
#if DEBUG
                else if (!result.success)
                {
                    foreach (SalesforceEnterprise.Error e in result.errors)
                    {
                        strErrors += String.Format("{0}\r\n", e.message);
                    }
                }
#endif
            }

#if DEBUG
            if (strErrors.Trim().Length > 0)
            {
                throw new Exception(String.Format("Fatal Error(s): {0}", strErrors));
            }
#endif
            return upsertResults;
        }

        public SalesforceEnterprise.SaveResult[] update(SalesforceEnterprise.sObject[] sObjectsToSave)
        {
            connect();

            SalesforceEnterprise.SaveResult[] SaveResults = { };
            Dictionary<int, List<SalesforceEnterprise.sObject>> usChunks = new Dictionary<int, List<SalesforceEnterprise.sObject>>();
            double lChunks = (double)sObjectsToSave.LongLength / 200.0;
            if (lChunks > 1)
            {
                int iChunk = 0;
                long lChunk = 0;
                for (long i = 0; i < sObjectsToSave.LongLength; i++)
                {
                    if (usChunks.ContainsKey(iChunk))
                    {
                        usChunks[iChunk].Add(sObjectsToSave[i]);
                    }
                    else
                    {
                        usChunks.Add(iChunk, new List<SalesforceEnterprise.sObject>());
                        usChunks[iChunk].Add(sObjectsToSave[i]);
                    }

                    lChunk++;
                    if (lChunk == 200)
                    {
                        lChunk = 0;
                        iChunk++;
                    }
                }

                foreach (KeyValuePair<int, List<SalesforceEnterprise.sObject>> kvpItem in usChunks)
                {
                    SaveResults = _SFService.update(kvpItem.Value.ToArray());
                }
            }
            else
            {
                SaveResults = _SFService.update(sObjectsToSave);
            }

#if RELEASE
            string strErrors = "";
#endif
            for (int i = 0; i < SaveResults.Length; i++)
            {
                SalesforceEnterprise.SaveResult result = SaveResults[i];
                if (result.success)
                {
                    SalesforceEnterprise.sObject insertedSObject = sObjectsToSave[i];
                    insertedSObject.Id = result.id;
                }
#if RELEASE
                else
                {
                    foreach (SalesforceEnterprise.Error e in result.errors)
                    {
                        strErrors += String.Format("{0}\r\n", e.message);
                    }
                }
#endif
            }

#if RELEASE
            if (strErrors.Trim().Length > 0)
            {
                throw new Exception(String.Format("Fatal Error(s): {0}", strErrors));
            }
#endif
            return SaveResults;
        }

        public SalesforceEnterprise.DeleteResult[] delete(string[] idsToDelete)
        {
            connect();
            return _SFService.delete(idsToDelete);
        }


    }
}
