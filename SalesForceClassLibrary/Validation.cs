﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SalesForceClassLibrary
{
    public static class SalesforceValidation
    {
        public static class Account
        {
            public static bool IsLikeAnEmail(object o)
            {
                bool b = false;
                if (o != null)
                {
                    if (o is String)
                    {
                        b = Regex.IsMatch(o.ToString(), @"(?:[A-Za-z0-9+&%\.-_]+@.+)");
                    }
                }
                return b;
            }
        }

        public static class Listserv
        {
            public enum DeliveryOptions { Daily_Digest, Immediate_Delivery };

            public static string GetDeliveryOptionWithDefault(SalesforceEnterprise.Online_Subscription__c Subscription, DeliveryOptions Default)
            {
                string strRetVal = "";
                if (Subscription != null && Subscription.Listserv_Delivery_Option__c != null)
                {
                    strRetVal = Subscription.Listserv_Delivery_Option__c.ToString();
                }
                else
                {
                    strRetVal = Enum.GetName(typeof(DeliveryOptions), Default).Replace("_", " ");
                }
                return strRetVal;
            }

            public static string GetDeliveryOptionForLyrisWithDefault(SalesforceEnterprise.Online_Subscription__c Subscription, DeliveryOptions Default)
            {
                string strRetVal = "";
                if (Subscription != null && Subscription.Listserv_Delivery_Option__c != null)
                {
                    strRetVal = Subscription.Listserv_Delivery_Option__c.ToString();
                }
                else
                {
                    strRetVal = Enum.GetName(typeof(DeliveryOptions), Default).Replace("_", " ");
                }
                if (strRetVal.ToLower() == "daily digest")
                {
                    strRetVal = "digest";
                }
                else if (strRetVal.ToLower() == "immediate delivery")
                {
                    strRetVal = "mail";
                }
                else
                {
                    strRetVal = "mail";
                }
                return strRetVal;
            }
        }

        public static object SafeGetObjectField(object o)
        {
            if (o != null)
            {
                return o;
            }
            else
            {
                return new object();
            }
        }
    }
}
