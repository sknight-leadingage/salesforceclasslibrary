﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SalesForceClassLibrary
{
    [Serializable]
    public class LACompany
    {
        public String SalesForceId { get; set; }
        public String LeadingAgeId { get; set; }
        public String Name { get; set; }        
        public String Street { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String PostalCode { get; set; }
        public String Phone { get; set; }
        public String Website { get; set; }

        public LACompany()
        {
        }

        public void getById(string Id)
        {

            // if logged in, and getting info for logged in user, retrieve from cookie
            if (CommonFunctions.isLoggedIn() && Id != "123456789")
            {
                Dictionary<String, String> attributes = CommonFunctions.getLoginCookie();

                if (attributes.ContainsKey("CompanySalesForceId")) { this.SalesForceId =  HttpUtility.HtmlDecode(attributes["CompanySalesForceId"]); } 
                if (attributes.ContainsKey("CompanyLeadingAgeId")) { this.LeadingAgeId =  HttpUtility.HtmlDecode(attributes["CompanyLeadingAgeId"]); } 
                if (attributes.ContainsKey("CompanyName")) { this.Name =  HttpUtility.HtmlDecode(attributes["CompanyName"]); }                
                if (attributes.ContainsKey("CompanyStreet")) { this.Street =  HttpUtility.HtmlDecode(attributes["CompanyStreet"]); }
                if (attributes.ContainsKey("CompanyCity")) { this.City =  HttpUtility.HtmlDecode(attributes["CompanyCity"]); }
                if (attributes.ContainsKey("CompanyState")) { this.State =  HttpUtility.HtmlDecode(attributes["CompanyState"]); }
                if (attributes.ContainsKey("CompanyPostalCode")) { this.PostalCode =  HttpUtility.HtmlDecode(attributes["CompanyPostalCode"]); }
                if (attributes.ContainsKey("CompanyPhone")) { this.Phone =  HttpUtility.HtmlDecode(attributes["CompanyPhone"]); } 
                if (attributes.ContainsKey("CompanyWebsite")) { this.Website = HttpUtility.HtmlDecode(attributes["CompanyWebsite"]); } 

            }

            // else query Salesforce for info // To Be Implemented Later
            else
            {

            }



        }
    }   
}
