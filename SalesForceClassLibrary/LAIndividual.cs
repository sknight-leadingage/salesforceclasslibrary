﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SalesForceClassLibrary
{
    [Serializable]
    public class LAIndividual
    {
        public String SalesForceId { get; set; }
        public String LeadingAgeId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String FullName { get; set; }
        public String Email { get; set; }
        public String Title { get; set; }
        public String Phone { get; set; }
        public bool isMember { get; set; }
        public bool isProviderMember { get; set; }
        public String Street { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String PostalCode { get; set; }
        public String CompanyName { get; set; }
        public String CompanyLeadingAgeId{ get; set; }
        public String CompanySalesForceId { get; set; }
        public String CompanyStreet { get; set; }
        public String CompanyCity { get; set; }
        public String CompanyState { get; set; }
        public String CompanyPostalCode { get; set; }
        public String CompanyProviderType { get; set; }
        public String ExhibitorEvents { get; set; }

        public LAIndividual()
        {

        }

        public string getID()
        {
            if (CommonFunctions.isLoggedIn())
            {
                Dictionary<String, String> attributes = CommonFunctions.getLoginCookie();

                return HttpUtility.HtmlDecode(attributes["Id"]); 
            }
            
            return null;

        }

        public string getLeadingAgeID()
        {
            if (CommonFunctions.isLoggedIn())
            {
                Dictionary<String, String> attributes = CommonFunctions.getLoginCookie();

                return HttpUtility.HtmlDecode(attributes["LeadingAgeId"]);
            }

            return null;

        }

        public void getById(string Id) 
        {
            
            // if logged in, and getting info for logged in user, retrieve from cookie
            if (CommonFunctions.isLoggedIn() && Id != "123456789")
            {
                Dictionary<String, String> attributes = CommonFunctions.getLoginCookie();

                if (attributes.ContainsKey("Id")) { this.SalesForceId = HttpUtility.HtmlDecode(attributes["Id"]); }
                if (attributes.ContainsKey("LeadingAgeId"))     { this.LeadingAgeId = HttpUtility.HtmlDecode(attributes["LeadingAgeId"]); }
                if (attributes.ContainsKey("FirstName"))        { this.FirstName = HttpUtility.HtmlDecode(attributes["FirstName"]); }
                if (attributes.ContainsKey("LastName"))         { this.LastName = HttpUtility.HtmlDecode(attributes["LastName"]); }
                if (attributes.ContainsKey("FullName"))         { this.FullName = HttpUtility.HtmlDecode(attributes["FullName"]); }
                if (attributes.ContainsKey("Email"))            { this.Email = HttpUtility.HtmlDecode(attributes["Email"]); }
                if (attributes.ContainsKey("Title"))            { this.Title = HttpUtility.HtmlDecode(attributes["Title"]); }
                if (attributes.ContainsKey("Phone"))            { this.Phone = HttpUtility.HtmlDecode(attributes["Phone"]); }
                if (attributes.ContainsKey("Member"))           { this.isMember = Convert.ToBoolean(attributes["Member"]); }
                if (attributes.ContainsKey("ProviderMember"))   { this.isProviderMember = Convert.ToBoolean(attributes["ProviderMember"]); }
                if (attributes.ContainsKey("Street"))           { this.Street = HttpUtility.HtmlDecode(attributes["Street"]); }
                if (attributes.ContainsKey("City"))             { this.City = HttpUtility.HtmlDecode(attributes["City"]); }
                if (attributes.ContainsKey("State"))            { this.State = HttpUtility.HtmlDecode(attributes["State"]); }
                if (attributes.ContainsKey("PostalCode"))       { this.PostalCode = HttpUtility.HtmlDecode(attributes["PostalCode"]); }
                if (attributes.ContainsKey("CompanyName"))      { this.CompanyName = HttpUtility.HtmlDecode(attributes["CompanyName"]); }
                if (attributes.ContainsKey("CompanyLeadingAgeId")) { this.CompanyLeadingAgeId = HttpUtility.HtmlDecode(attributes["CompanyLeadingAgeId"]); } 
                if (attributes.ContainsKey("CompanySalesForceId")) { this.CompanySalesForceId = HttpUtility.HtmlDecode(attributes["CompanySalesForceId"]); }
                if (attributes.ContainsKey("CompanyProviderType")) { this.CompanyProviderType = HttpUtility.HtmlDecode(attributes["CompanyProviderType"]); }
                if (attributes.ContainsKey("ExhibitorEvents")) { this.ExhibitorEvents = HttpUtility.HtmlDecode(attributes["ExhibitorEvents"]); } 
            }

            // else query Salesforce for info // To Be Implemented Later
            else
            {
                
            }

            

        }
        
    }
}
