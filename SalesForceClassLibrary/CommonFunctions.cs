﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SalesForceClassLibrary
{
    static public class CommonFunctions
    {
        static public Boolean isLoggedIn()
        {            

            if (HttpContext.Current.Request.Cookies["Attributes"] != null)
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.Request.Cookies["Attributes"].Value))
                return true;
            }

            return false;
        }


        static public Boolean isMember()
        {
           Boolean MemberCheck = false;
           Dictionary<String, String> attributes = getLoginCookie();

           if (attributes != null)
           {
               MemberCheck = Convert.ToBoolean(attributes["Member"]); 

               return MemberCheck;
           }          
           return false;
        }

        static public Boolean isProviderMember()
        {
            Boolean ProviderMemberCheck = false;
            Dictionary<String, String> attributes = getLoginCookie();

            if (attributes != null)
            {
                ProviderMemberCheck = Convert.ToBoolean(attributes["ProviderMember"]);

                //If Not Provider Member, let's check to see if they are attached to an MSO and if any of the MSO children are a provider member

                if (!ProviderMemberCheck)
                {
                    LAIndividual _Ind = new LAIndividual();  
                    string LA_ID = _Ind.getLeadingAgeID();
                    string strSOQL = "SELECT NU__Account__r.Name, NU__Account__r.LeadingAge_ID__c, NU__Account__r.National_Provider_Id__c FROM NU__Affiliation__c WHERE NU__ParentAccount__c IN (SELECT NU__PrimaryAffiliation__c FROM Account WHERE LeadingAge_ID__c = '" + LA_ID + "') AND NU__Status__c = 'Active' AND NU__Account__r.RecordTypeId = '012d0000000guGXAAY' ORDER BY Parent_Name__c";

                    Querier<SalesforceEnterprise.Account> myQuerrier = new Querier<SalesforceEnterprise.Account>();
                    List<SalesforceEnterprise.Account> ChildAccounts = new List<SalesforceEnterprise.Account>();
                    ChildAccounts = myQuerrier.Query(@strSOQL);

                    if (ChildAccounts != null)
                    {
                        foreach (SalesforceEnterprise.Account a in ChildAccounts)
                        {
                            if (a.Provider_Member__c == "Yes")
                            {
                                ProviderMemberCheck = true;
                                break;
                            }
                        }
                    }
                }

                return ProviderMemberCheck;
            }
            return false;
        }

        public static Boolean isOnEvent(string EventId)
        {
            Boolean bRetVal = false;
            Dictionary<String, String> attributes = getLoginCookie();

            if (attributes != null)
            {
                LAIndividual _Ind = new LAIndividual();
                string LA_ID = _Ind.getLeadingAgeID();
                string strSOQL = "";

                if (EventId.Contains(";") || EventId.Contains(","))
                {
                    string strSOQLEventPart = "";
                    string strTempEvId = EventId;
                    strTempEvId = strTempEvId.Replace(",", ";");
                    string[] strEventIds = strTempEvId.Split(";".ToCharArray());
                    foreach(string strEvId in strEventIds)
                    {
                        if (string.IsNullOrEmpty(strSOQLEventPart))
                        {
                            strSOQLEventPart = String.Format("'{0}'", strEvId.Trim());
                        }
                        else
                        {
                            strSOQLEventPart += String.Format(",'{0}'", strEvId.Trim());
                        }
                    }
                    strSOQL = String.Format("SELECT Id FROM NU__Registration2__c WHERE NU__Account__r.LeadingAge_ID__c = '{0}' AND NU__Event__c IN({1}) AND NU__Status__c = 'Active'", LA_ID, strSOQLEventPart);
                }
                else
                {
                    strSOQL = String.Format("SELECT Id FROM NU__Registration2__c WHERE NU__Account__r.LeadingAge_ID__c = '{0}' AND NU__Event__c = '{1}' AND NU__Status__c = 'Active'", LA_ID, EventId);
                }

                Querier<SalesforceEnterprise.NU__Registration2__c> myQuerrier = new Querier<SalesforceEnterprise.NU__Registration2__c>();
                List<SalesforceEnterprise.NU__Registration2__c> regRecords = new List<SalesforceEnterprise.NU__Registration2__c>();
                regRecords = myQuerrier.Query(@strSOQL);

                if (regRecords != null && regRecords.Count > 0)
                {
                    bRetVal = true;
                }
            }
            return bRetVal;
        }

        public static Boolean isExhibitor()
        {
            Boolean ExhibitorMemberCheck = false;
            Dictionary<String, String> attributes = getLoginCookie();

            if (attributes != null && attributes.ContainsKey("ExhibitorEvents"))
            {
                if (!string.IsNullOrEmpty(attributes["ExhibitorEvents"].Trim()))
                {
                    ExhibitorMemberCheck = true;
                }
            }
            return ExhibitorMemberCheck;
        }

        static public Dictionary<String, String> getLoginCookie()
        {           
            String cookieValue = String.Empty;

            if (HttpContext.Current.Request.Cookies["Attributes"] != null)
            {
                cookieValue = HttpContext.Current.Request.Cookies["Attributes"].Value;

                Dictionary<String, String> attributes = new Dictionary<String, String>();
                List<String> temp = cookieValue.Trim('|').Replace("\"", String.Empty).Split('|').ToList();

                if (temp != null && temp.Count > 0)
                {
                    foreach (String str in temp)
                    {
                        attributes.Add(str.Split(':')[0], str.Split(':')[1]);
                    }
                }
                return attributes;
            }           

            return null;
        }

        public class WebsiteActions
        {
            public class WebsiteActionData
            {
                public string ActionCode { get; set; }
                public string ActionDescription { get; set; }
                public string IndividualId { get; set; }
                public string ReferenceAccount { get; set; }
                public string ExtendedType { get; set; }
                public GroupByField GroupBy { get; set; }
                public string ReferenceField1 { get; set; }
                public string ReferenceField2 { get; set; }
                public bool? ReferenceField1Exists { get; set; }
                public bool? ReferenceField2Exists { get; set; }

                public enum GroupByField
                {
                    AccountIdAndActionCode,
                    AccountIdAndActionCodeAndExtendedType
                }

                public WebsiteActionData()
                {
                    this._constructGeneric("", "");
                }

                public WebsiteActionData(string ActionCode, string ActionDescription)
                {
                    this._constructGeneric(ActionCode, ActionDescription);
                }

                public WebsiteActionData(string ActionCode, string ActionDescription, string IndividualId)
                {
                    this._constructGeneric(ActionCode, ActionDescription);
                    this.IndividualId = IndividualId;
                }

                public WebsiteActionData(string ActionCode, string ActionDescription, string IndividualId, string ReferenceAccount)
                {
                    this._constructGeneric(ActionCode, ActionDescription);
                    this.IndividualId = IndividualId;
                    this.ReferenceAccount = ReferenceAccount;
                }

                public WebsiteActionData(string ActionCode, string ActionDescription, string IndividualId, string ReferenceAccount, string ExtendedType)
                {
                    this._constructGeneric(ActionCode, ActionDescription);
                    this.IndividualId = IndividualId;
                    this.ReferenceAccount = ReferenceAccount;
                    this.ExtendedType = ExtendedType;
                }

                public WebsiteActionData(string ActionCode, string ActionDescription, string IndividualId, string ReferenceAccount, string ExtendedType, GroupByField GroupBy)
                {
                    this._constructGeneric(ActionCode, ActionDescription);
                    this.IndividualId = IndividualId;
                    this.ReferenceAccount = ReferenceAccount;
                    this.ExtendedType = ExtendedType;
                    this.GroupBy = GroupBy;
                }

                public WebsiteActionData(string ActionCode, string ActionDescription, string IndividualId, string ReferenceAccount, string ExtendedType, GroupByField GroupBy, string ReferenceField1)
                {
                    this._constructGeneric(ActionCode, ActionDescription);
                    this.IndividualId = IndividualId;
                    this.ReferenceAccount = ReferenceAccount;
                    this.ExtendedType = ExtendedType;
                    this.GroupBy = GroupBy;
                    this.ReferenceField1 = ReferenceField1;
                }

                public WebsiteActionData(string ActionCode, string ActionDescription, string IndividualId, string ReferenceAccount, string ExtendedType, GroupByField GroupBy, string ReferenceField1, string ReferenceField2)
                {
                    this._constructGeneric(ActionCode, ActionDescription);
                    this.IndividualId = IndividualId;
                    this.ReferenceAccount = ReferenceAccount;
                    this.ExtendedType = ExtendedType;
                    this.GroupBy = GroupBy;
                    this.ReferenceField1 = ReferenceField1;
                    this.ReferenceField2 = ReferenceField2;
                }

                public WebsiteActionData(string ActionCode, string ActionDescription, string IndividualId, string ReferenceAccount, string ExtendedType, GroupByField GroupBy, string ReferenceField1, string ReferenceField2, bool ReferenceField1Exists, bool ReferenceField2Exists)
                {
                    this._constructGeneric(ActionCode, ActionDescription);
                    this.IndividualId = IndividualId;
                    this.ReferenceAccount = ReferenceAccount;
                    this.ExtendedType = ExtendedType;
                    this.GroupBy = GroupBy;
                    this.ReferenceField1 = ReferenceField1;
                    this.ReferenceField2 = ReferenceField2;
                    this.ReferenceField1Exists = ReferenceField1Exists;
                    this.ReferenceField2Exists = ReferenceField2Exists;
                }

                internal void _constructGeneric(string ActionCode, string ActionDescription)
                {
                    this.ActionCode = ActionCode;
                    this.ActionDescription = ActionDescription;
                    this.GroupBy = GroupByField.AccountIdAndActionCode;
                }
            }

            public static void InsertWebsiteAction(WebsiteActionData Action)
            {
                SalesforceEnterprise.Website_Action__c WA = new SalesforceEnterprise.Website_Action__c();
                LAIndividual _Ind = new LAIndividual();
                string SfId = null;

                if (Action.IndividualId == null || string.IsNullOrEmpty(Action.IndividualId))
                {
                    
                    // Cookie Value does not contain correct SalesForceID (or it is being encryped and we are not aware of it)
                    // Workaround - Get SFID from LeadingAgeID
                    Querier<SalesforceEnterprise.Account> qGetSfID = new Querier<SalesforceEnterprise.Account>();
                    //using the logged in user
                    string qGetSfID_SOQL = String.Format("SELECT id from Account Where LeadingAge_ID__c = '{0}'", _Ind.getLeadingAgeID());

                    List<SalesforceEnterprise.Account> myAccounts = new List<SalesforceEnterprise.Account>();
                    myAccounts = qGetSfID.Query(@qGetSfID_SOQL);

                    if (myAccounts.Count == 1)
                    {
                        SfId = myAccounts[0].Id;
                    }
                }
                else
                {
                    SfId = Action.IndividualId; // use passed paramater if there is no logged in user (currently for Quality Metrics Web Service)
                }

                Querier<SalesforceEnterprise.Website_Action__c> myQuerrier = new Querier<SalesforceEnterprise.Website_Action__c>();
                string mySOQL = "";

                switch (Action.GroupBy)
                {
                    case WebsiteActionData.GroupByField.AccountIdAndActionCode:
                        mySOQL = String.Format("SELECT id, Visits__c FROM Website_Action__c WHERE Account__r.Id = '{0}' and Code__c = '{1}'", SfId, Action.ActionCode);
                        break;
                    case WebsiteActionData.GroupByField.AccountIdAndActionCodeAndExtendedType:
                        mySOQL = String.Format("SELECT id, Visits__c FROM Website_Action__c WHERE Account__r.Id = '{0}' and Code__c = '{1}' AND Extended_Type__c = '{2}'", SfId, Action.ActionCode, Action.ExtendedType);
                        break;
                    default:
                        mySOQL = String.Format("SELECT id, Visits__c FROM Website_Action__c WHERE Account__r.Id = '{0}' and Code__c = '{1}'", SfId, Action.ActionCode);
                        break;
                }

                List<SalesforceEnterprise.Website_Action__c> myWebActions = new List<SalesforceEnterprise.Website_Action__c>();
                myWebActions = myQuerrier.Query(@mySOQL);

                if (myWebActions.Count == 0)
                {
                    //First Action, insert new row                

                    WA.Account__c = SfId;  //workaround GC 1/16/14
                    WA.Code__c = Action.ActionCode;
                    WA.Description__c = Action.ActionDescription;
                    WA.Visits__c = 1;
                    WA.Visits__cSpecified = true;
                    WA.First_Visit_Date__c = DateTime.Now.ToUniversalTime();
                    WA.First_Visit_Date__cSpecified = true;
                    WA.Last_Visit_Date__c = DateTime.Now.ToUniversalTime();
                    WA.Last_Visit_Date__cSpecified = true;
                    if (Action.ReferenceAccount != null && !string.IsNullOrEmpty(Action.ReferenceAccount))
                    {
                        WA.Reference_Account__c = Action.ReferenceAccount;
                    }
                    if (Action.ExtendedType != null && !string.IsNullOrEmpty(Action.ExtendedType))
                    {
                        WA.Extended_Type__c = Action.ExtendedType;
                    }
                    if (Action.ReferenceField1 != null && !string.IsNullOrEmpty(Action.ReferenceField1))
                    {
                        WA.Reference_Field_1__c = Action.ReferenceField1;
                    }
                    if (Action.ReferenceField2 != null && !string.IsNullOrEmpty(Action.ReferenceField2))
                    {
                        WA.Reference_Field_2__c = Action.ReferenceField2;
                    }
                    if (Action.ReferenceField1Exists.HasValue)
                    {
                        WA.Reference_Field_1_Exists__c = Action.ReferenceField1Exists.Value;
                        WA.Reference_Field_1_Exists__cSpecified = true;
                    }
                    if (Action.ReferenceField2Exists.HasValue)
                    {
                        WA.Reference_Field_2_Exists__c = Action.ReferenceField2Exists.Value;
                        WA.Reference_Field_2_Exists__cSpecified = true;
                    }
                }
                else
                {
                    //Subsequent Action, Update Visit Count and last visit date
                    WA = myWebActions[0];

                    WA.Visits__c = WA.Visits__c + 1;
                    WA.Visits__cSpecified = true;
                    WA.Last_Visit_Date__c = DateTime.Now.ToUniversalTime();
                    WA.Last_Visit_Date__cSpecified = true;

                    if (Action.ReferenceAccount != null && !string.IsNullOrEmpty(Action.ReferenceAccount))
                    {
                        WA.Reference_Account__c = Action.ReferenceAccount;
                    }
                    if (Action.ExtendedType != null && !string.IsNullOrEmpty(Action.ExtendedType))
                    {
                        WA.Extended_Type__c = Action.ExtendedType;
                    }
                    if (Action.ReferenceField1 != null && !string.IsNullOrEmpty(Action.ReferenceField1))
                    {
                        WA.Reference_Field_1__c = Action.ReferenceField1;
                    }
                    if (Action.ReferenceField2 != null && !string.IsNullOrEmpty(Action.ReferenceField2))
                    {
                        WA.Reference_Field_2__c = Action.ReferenceField2;
                    }
                    if (Action.ReferenceField1Exists.HasValue)
                    {
                        WA.Reference_Field_1_Exists__c = Action.ReferenceField1Exists.Value;
                        WA.Reference_Field_1_Exists__cSpecified = true;
                    }
                    if (Action.ReferenceField2Exists.HasValue)
                    {
                        WA.Reference_Field_2_Exists__c = Action.ReferenceField2Exists.Value;
                        WA.Reference_Field_2_Exists__cSpecified = true;
                    }
                }

                EnterpriseService eServe = new EnterpriseService();
                eServe.upsert(new SalesforceEnterprise.Website_Action__c[] { WA });
            }

            public static int QueryWebsiteActionVisits(string strExtendedType, string strCode1, string strCode2)
            {
                int iRetVal = 0;
                List<SalesforceEnterprise.Website_Action__c> wActions = QueryWebsiteAction(strExtendedType, strCode1, strCode2);
                if (wActions.Count > 0)
                {
                    foreach (SalesforceEnterprise.Website_Action__c wAction in wActions)
                    {
                        if (wAction.Visits__c.HasValue && wAction.Visits__c.Value > iRetVal)
                        {
                            iRetVal = (int)wAction.Visits__c.Value;
                        }
                    }
                }
                return iRetVal;
            }

            public static List<SalesforceEnterprise.Website_Action__c> QueryWebsiteAction(string strExtendedType, string strCode1, string strCode2)
            {
                LAIndividual _Ind = new LAIndividual();
                Querier<SalesforceEnterprise.Website_Action__c> qGetSfID = new Querier<SalesforceEnterprise.Website_Action__c>();
                //using the logged in user
                string qGetSfID_SOQL = String.Format("SELECT Id, Account__c, Code__c, Description__c, First_Visit_Date__c, Last_Visit_Date__c, Owner__c, Visits__c, Reference_Field_1__c, Reference_Field_2__c, Reference_Account__c, Extended_Type__c FROM Website_Action__c WHERE Account__r.LeadingAge_ID__c = '{0}' AND Extended_Type__c = '{1}' AND (Code__c = '{2}' OR Code__c = '{3}')", _Ind.getLeadingAgeID(), strExtendedType, strCode1, strCode2);

                List<SalesforceEnterprise.Website_Action__c> wActions = new List<SalesforceEnterprise.Website_Action__c>();
                wActions = qGetSfID.Query(@qGetSfID_SOQL);

                return wActions;
            }
        }

        [Obsolete("insertWebsiteAction is deprecated, please see the WebsiteActions class instead.")]
        static public void insertWebsiteAction(string actionCode, string actionDescription, string ind_id = null)
        {

            SalesforceEnterprise.Website_Action__c WA = new SalesforceEnterprise.Website_Action__c();
            LAIndividual _Ind = new LAIndividual();            
            string SfId; 

            if (ind_id == null)
            {
                //using the logged in user
                SfId = getSalesforceIdfromLeadingAgeID(_Ind.getLeadingAgeID());                
            }
            else
            {
                SfId = ind_id; // use passed paramater if there is no logged in user (currently for Quality Metrics Web Service)
            }

            Querier<SalesforceEnterprise.Website_Action__c> myQuerrier = new Querier<SalesforceEnterprise.Website_Action__c>();
            string mySOQL = String.Format("SELECT id, Visits__c FROM Website_Action__c WHERE Account__r.Id = '{0}' and Code__c = '{1}'", SfId, actionCode);

            List<SalesforceEnterprise.Website_Action__c> myWebActions = new List<SalesforceEnterprise.Website_Action__c>();
            myWebActions = myQuerrier.Query(@mySOQL);

            if (myWebActions.Count == 0)
            {
                //First Action, insert new row                
                
                WA.Account__c = SfId;  //workaround GC 1/16/14
                WA.Code__c = actionCode;
                WA.Description__c = actionDescription;
                WA.Visits__c = 1;
                WA.Visits__cSpecified = true;
                WA.First_Visit_Date__c = DateTime.Now.ToUniversalTime();
                WA.First_Visit_Date__cSpecified = true;
                WA.Last_Visit_Date__c = DateTime.Now.ToUniversalTime();
                WA.Last_Visit_Date__cSpecified = true;
            }
            else
            {
                //Subsequent Action, Update Visit Count and last visit date
                WA = myWebActions[0];

                WA.Visits__c = WA.Visits__c + 1;
                WA.Visits__cSpecified = true;
                WA.Last_Visit_Date__c = DateTime.Now.ToUniversalTime();
                WA.Last_Visit_Date__cSpecified = true;
            }

            EnterpriseService eServe = new EnterpriseService();
            eServe.upsert(new SalesforceEnterprise.Website_Action__c[] {WA} );
        }

        // Cookie Value does not contain correct SalesForceID (or it is being encryped and we are not aware of it)
        // Workaround - Get SFID from LeadingAgeID
        static private string getSalesforceIdfromLeadingAgeID(string LAID)
        {

            Querier<SalesforceEnterprise.Account> myQuerrier = new Querier<SalesforceEnterprise.Account>();
            string mySOQL = "SELECT id from Account Where LeadingAge_ID__c = '" + LAID + "'";
            
            List<SalesforceEnterprise.Account> myAccounts = new List<SalesforceEnterprise.Account>();
            myAccounts = myQuerrier.Query(@mySOQL);

            if (myAccounts.Count == 1)
            {
                return myAccounts[0].Id;
            }
            return null;        
        }


        public struct NimbleTranslate
        {
            public struct ToLyris
            {
                public static string ValueOfDeliveryOption(string NimbleName)
                {
                    string strRetVal = "mail";
                    var values = Enum.GetValues(typeof(SalesforceValidation.Listserv.DeliveryOptions));
                    foreach (SalesforceValidation.Listserv.DeliveryOptions o in values)
                    {
                        if (NimbleName.ToLower().Trim() == Enum.GetName(typeof(SalesforceValidation.Listserv.DeliveryOptions), o).Replace("_", " ").ToLower().Trim())
                        {
                            if (o == SalesforceValidation.Listserv.DeliveryOptions.Daily_Digest)
                            {
                                strRetVal = "digest";
                                break;
                            }
                        }
                    }
                    return strRetVal;
                }

                public static string ValueOfStatus(string NimbleStatus)
                {
                    string strRetVal = "";
                    if (NimbleStatus.Length > 0)
                    {
                        strRetVal = NimbleStatus[0].ToString().ToUpper();
                    }
                    return strRetVal;
                }

                public static string ValueOfStatus(object o)
                {
                    string strRetVal = "";
                    if (o != null)
                    {
                        if (o.ToString().Length > 0)
                        {
                            strRetVal = o.ToString()[0].ToString().ToUpper();
                        }
                    }
                    return strRetVal;
                }
            }
        }
    }
}
