﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace SalesForceClassLibrary
{
    public class LeadingAgeEncryption
    {
        private byte[] IVector = { 27, 9, 45, 27, 0, 72, 171, 54 };

        public string Encrypt(string str, string PublicKey)
        {
            //Create a new RSA key. This key will encrypt a symmetric key,
            //which will then be imbedded in the XML document.

            //Get a byte array of the str as encryption works on byte blocks
            ASCIIEncoding enc = new ASCIIEncoding();
            byte[] byteData = enc.GetBytes(str);

            //Create encryption object
            TripleDESCryptoServiceProvider tDes = new TripleDESCryptoServiceProvider();

            //Specify Initialisation Vector as encryption key to use
            tDes.IV = IVector;
            MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
            tDes.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(PublicKey));

            //Adds key and IVector to Encrypt object
            CryptoAPITransform ITransform = (CryptoAPITransform)tDes.CreateEncryptor();

            return Convert.ToBase64String(ITransform.TransformFinalBlock(byteData, 0, byteData.Length));
        }

        public string Decrypt(string base64_str, string PublicKey)
        {
            //Perform as decrypt of encytpted data with above method

            //Get byte array from string
            byte[] encData = Convert.FromBase64String(base64_str);

            //Create encryption object
            TripleDESCryptoServiceProvider tDes = new TripleDESCryptoServiceProvider();

            //Specify Initialisation Vector as encryption key to use
            tDes.IV = IVector;
            MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
            tDes.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(PublicKey));

            //Adds key and IVector to decrypt object
            CryptoAPITransform ITransform = (CryptoAPITransform)tDes.CreateDecryptor();

            return Encoding.ASCII.GetString(ITransform.TransformFinalBlock(encData, 0, encData.Length));
        }

        public class Encryption
        {
            public static string EncryptString(string StringToExcrypt, string PublicKey)
            {
                byte[] byKey = { };
                byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0xAA, 0x60, 0x50, 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0xAA, 0x60, 0x50 };

                try
                {
                    byKey = System.Text.Encoding.UTF8.GetBytes(PublicKey.Substring(0, 8));

                    DESCryptoServiceProvider des = new DESCryptoServiceProvider();

                    byte[] inputByteArray = Encoding.UTF8.GetBytes(StringToExcrypt);
                    MemoryStream ms = new MemoryStream();

                    CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
                    cs.Write(inputByteArray, 0, inputByteArray.Length);
                    cs.FlushFinalBlock();

                    //Manually Escape +, which indicate a space in a query string.
                    string TempString = "";
                    TempString = Convert.ToBase64String(ms.ToArray());
                    if (TempString.Contains("+"))
                    {
                        TempString = TempString.Replace("+", "%2B");
                    }
                    return TempString;
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }

            public static string DecryptString(string EncryptedValue, string PublicKey)
            {
                byte[] byKey = { };
                byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0xAA, 0x60, 0x50, 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0xAA, 0x60, 0x50 };
                byte[] inputByteArray = new byte[EncryptedValue.Length];

                try
                {
                    //The %2B should already be covnerted back to a +, but incase its not do it here
                    if (EncryptedValue.Contains("%2B") || EncryptedValue.Contains("%2b"))
                    {
                        EncryptedValue = EncryptedValue.Replace("%2B", "+");
                        EncryptedValue = EncryptedValue.Replace("%2b", "+");
                    }

                    byKey = System.Text.Encoding.UTF8.GetBytes(PublicKey.Substring(0, 8));
                    DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                    inputByteArray = Convert.FromBase64String(EncryptedValue);

                    MemoryStream ms = new MemoryStream();
                    CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);

                    cs.Write(inputByteArray, 0, inputByteArray.Length);
                    cs.FlushFinalBlock();
                    System.Text.Encoding encoding = System.Text.Encoding.UTF8;

                    return encoding.GetString(ms.ToArray());
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
        }
    }
}
